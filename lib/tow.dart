import 'package:flutter/material.dart';

class Second extends StatefulWidget {
  const Second({super.key});

  @override
  State<Second> createState() => _SecondState();
}

class _SecondState extends State<Second> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 248, 247, 247),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Image.asset("assets/new.png"),
                    const Spacer(),
                    Image.asset(
                      "assets/action3.png",
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Stack(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: Image.asset("assets/action2.png"),
                        ),
                        Positioned(
                          right: 1,
                          child: Container(
                            height: 17,
                            width: 17,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "2",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Container(
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.add),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  children: [
                    const Column(
                      children: [
                        Text(
                          "Welcome, Mypcot !!",
                          style: TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "here is your dashboard....",
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    const Spacer(),
                    Column(
                      children: [
                        Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: const Icon(
                            Icons.search,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Container(
                              height: 250,
                              width: 380,
                              decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 34, 151, 246),
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white),
                                        child: Image.asset("assets/first.png"),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      SizedBox(
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                Colors.red,
                                              ),
                                            ),
                                            onPressed: () {},
                                            child: Text(
                                              "Orders",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            right: 40,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 35),
                                  child: Container(
                                    height: 100,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: const Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "You have 3 active",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 65)),
                                            Text(
                                              "order from",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 70,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.amber,
                                    radius: 25.0,
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 110,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    radius: 25.0,
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 150,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.blue,
                                    radius: 25.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Positioned(
                            right: 70,
                            top: 120,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 80,
                                    width: 150,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: const Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "02 ",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w800,
                                                fontSize: 15,
                                              ),
                                            ),
                                            Text(
                                              "pending",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 25, 25, 25),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 13,
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 40)),
                                            Text(
                                              "order from",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 45,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.amber,
                                    radius: 22,
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 80,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.black,
                                    radius: 22,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Container(
                              height: 250,
                              width: 380,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 223, 206, 48),
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white),
                                        child: Image.asset("assets/second.png"),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      SizedBox(
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                Colors.blue,
                                              ),
                                            ),
                                            onPressed: () {},
                                            child: Text(
                                              "Subscriptions",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            right: 40,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 35),
                                  child: Container(
                                    height: 100,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: const Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "03",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "deliveries",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 15),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            right: 70,
                            top: 100,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 50),
                                  child: Container(
                                    height: 66,
                                    width: 120,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: const Padding(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 15),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "10 ",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                "active",
                                                style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 25, 25, 25),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 13,
                                                ),
                                              )
                                            ],
                                          ),
                                          // SizedBox(
                                          //   height: 9,
                                          // ),
                                          Row(
                                            children: [
                                              Text(
                                                "Subscription",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 70,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.amber,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 140,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 210,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.pink,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            right: 40,
                            top: 180,
                            child: Stack(
                              children: [
                                Container(
                                  height: 66,
                                  width: 120,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.only(left: 10, top: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              "119 ",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w800,
                                                fontSize: 20,
                                              ),
                                            ),
                                            Text(
                                              "pending",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 25, 25, 25),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 13,
                                              ),
                                            )
                                          ],
                                        ),
                                        // SizedBox(
                                        //   height: 9,
                                        // ),
                                        Row(
                                          children: [
                                            Text(
                                              "Deliveries",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Container(
                              height: 250,
                              width: 380,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 47, 255, 5),
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white),
                                        child: Image.asset("assets/three.png"),
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      SizedBox(
                                        child: ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStatePropertyAll(
                                                Color.fromARGB(
                                                    207, 234, 26, 26),
                                              ),
                                            ),
                                            onPressed: () {},
                                            child: Text(
                                              "View Customers",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            right: 40,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 35),
                                  child: Container(
                                    height: 100,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      color: Color.fromARGB(207, 234, 26, 26),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: const Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "15",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "New customers",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 15),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            right: 30,
                            top: 100,
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 50),
                                  child: Container(
                                    height: 66,
                                    width: 120,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: const Padding(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 15),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "1.8 %",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 30,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 70,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.amber,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 140,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            top: 65,
                            left: 210,
                            right: 40,
                            child: CircleAvatar(
                              backgroundColor: Colors.pink,
                              radius: 25.0,
                            ),
                          ),
                          Positioned(
                            right: 70,
                            top: 180,
                            child: Stack(
                              children: [
                                Container(
                                  height: 66,
                                  width: 115,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.only(left: 10, top: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              "10 ",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w800,
                                                fontSize: 20,
                                              ),
                                            ),
                                            Text(
                                              "Active",
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 25, 25, 25),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 13,
                                              ),
                                            )
                                          ],
                                        ),
                                        // SizedBox(
                                        //   height: 9,
                                        // ),
                                        Row(
                                          children: [
                                            Text(
                                              "Customers",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 195,
                            left: 220,
                            right: 0,
                            child: CircleAvatar(
                              backgroundColor: Colors.amber,
                              radius: 18.0,
                            ),
                          ),
                          Positioned(
                            top: 195,
                            left: 270,
                            right: 0,
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                              radius: 18.0,
                            ),
                          ),
                          Positioned(
                            top: 195,
                            left: 320,
                            right: 0,
                            child: CircleAvatar(
                              backgroundColor: Colors.pink,
                              radius: 18.0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "January, 23 2021",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        "Today",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 30,
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          offset: Offset(0, 2),
                          blurRadius: 5,
                          color: Color.fromARGB(255, 165, 164, 164),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "TIMELINE",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Color.fromARGB(255, 63, 62, 62),
                          ),
                        ),
                        Icon(
                          Icons.arrow_drop_down_outlined,
                          size: 30,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 30,
                    width: 120,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 3),
                          blurRadius: 3,
                          spreadRadius: 1,
                          color: Color.fromARGB(255, 202, 200, 200),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/calender.png"),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "JAN, 2021",
                          style: TextStyle(
                              color: Color.fromARGB(255, 63, 62, 62),
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "MON",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "20",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "TUE",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "21",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "WED",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "22",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "THU",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "23",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "FRI",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "24",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "SAT",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "25",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(11.0),
                      child: Column(
                        children: [
                          Text(
                            "SUN",
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 18,
                              color: Color.fromARGB(255, 155, 155, 155),
                            ),
                          ),
                          Text(
                            "26",
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 180,
                width: 360,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "New Order created",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "New Order created with Order",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 17,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "09:00 AM",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 17,
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward,
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 80,
                            width: 80,
                            child: Image.asset("assets/first.png"),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromARGB(255, 62, 10, 84),
        shape: CircleBorder(),
        onPressed: () {},
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color.fromARGB(255, 244, 243, 243),
        shape: CircularNotchedRectangle(),
        child: Row(
          children: [
            Expanded(
                child: IconButton(
              onPressed: () {},
              icon: Image.asset(
                "assets/home.png",
                height: 800,
                width: 800,
              ),
            )),
            Expanded(
                child: IconButton(
              onPressed: () {},
              icon: Image.asset(
                "assets/home.png",
                height: 800,
                width: 800,
              ),
            )),
            Expanded(
                child: IconButton(
              onPressed: () {},
              icon: Image.asset(
                "assets/people.png",
                color: Color.fromARGB(255, 63, 62, 62),
              ),
            )),
            Expanded(
                child: IconButton(
                    onPressed: () {},
                    icon: Image.asset(
                      "assets/khata.png",
                      color: Color.fromARGB(255, 63, 62, 62),
                    ))),
            Expanded(
              child: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  "assets/order.png",
                  color: Color.fromARGB(255, 63, 62, 62),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
