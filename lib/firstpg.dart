import 'dart:math';

import 'package:flutter/material.dart';

class One extends StatefulWidget {
  const One({Key? key}) : super(key: key);

  @override
  State<One> createState() => OneState();
}

class OneState extends State<One> {
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromARGB(255, 62, 10, 84),
        shape: CircleBorder(),
        onPressed: () {},
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: Container(
        height:
            70, // Adjust this value to increase or decrease the height of the notch
        child: BottomAppBar(
          color: Colors.transparent,
          shape: CircularNotchedRectangle(),
          child: Row(
            children: [
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    "assets/home.png",
                    height: 800,
                    width: 800,
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Container(
                    height: 900, // Adjust the height as needed
                    width: 25, // Adjust the width as needed
                    child: Image.asset(
                      "assets/home.png",

                      fit: BoxFit.fill, // Maintain aspect ratio
                      // Optional color
                    ),
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    "assets/khata.png",
                    color: Color.fromARGB(255, 63, 62, 62),
                  ),
                ),
              ),
              Expanded(
                child: IconButton(
                  onPressed: () {},
                  icon: Image.asset(
                    "assets/order.png",
                    color: Color.fromARGB(255, 63, 62, 62),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
